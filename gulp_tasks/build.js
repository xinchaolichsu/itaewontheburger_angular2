module.exports = [
    [
        'jshint',
        'jshint-website',
        'html2js-website',
        'jshint-admin',
        'html2js',
        'html2js-admin',
        'uglify-website',
        'uglify-admin',
        'uglify',
        'minify-css-admin',
        'minify-css-website',
        'minify-css',
    ]
];
