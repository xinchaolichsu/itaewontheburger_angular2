var gulp = require('gulp');
var watch = require('gulp-watch');
module.exports = function() {
    watch(['webapp/js/app/**/*.js', 'webapp/js/api/**/*.js', 'webapp/js/models/**/*.js'], function() {
        gulp.run([
            'jshint',
            'browserify'
        ]);
    });

    watch('webapp/js/app/**/*.tpl.html', function() {
        gulp.run([
            'html2js',
            'browserify'
        ]);
    });

    gulp.watch('./webapp/css/scss/app/**/*.scss', ['concat']);
}
