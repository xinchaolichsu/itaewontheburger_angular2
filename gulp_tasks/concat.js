var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = [
    ['sass'],
    function() {
        gulp.src([
                './webapp/bower_components/bootstrap/dist/css/bootstrap.css',
                './webapp/bower_components/font-awesome/css/font-awesome.css',
                './webapp/bower_components/animate.css/animate.min.css',
                './public/assets/css/app.css',
                './webapp/bower_components/slider-revolution/src/css/settings.css'
            ])
            .pipe(concat("app.all.css"))
            .pipe(gulp.dest('public/assets/css'));
    }
];
