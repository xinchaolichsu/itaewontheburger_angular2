import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';

//============ route ========================
import { AppRoutingModule } from './app.routing';

//===========them ChildComponent child vao appModule===========
import { MenuComponent } from './frontend/menu/index';
import { HomepageComponent } from './frontend/homepage/index';


@NgModule({
  imports:      [ BrowserModule, AppRoutingModule, FormsModule, HttpModule ],
  declarations: [ AppComponent, MenuComponent, HomepageComponent ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
