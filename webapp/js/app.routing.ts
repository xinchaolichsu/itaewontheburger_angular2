import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//==================== import component ====================
import { MenuComponent } from './frontend/menu/index'; 
import { HomepageComponent } from './frontend/homepage/index';
 
const appRoutes: Routes = [
    { path: '', component: HomepageComponent },
    { path: 'menu', component: MenuComponent },
];

/**
 * import them config route vao RouteModule
 * export RouteModule ra ngoai de goi tu noi khac
 */

@NgModule ({
	imports: [RouterModule.forRoot(appRoutes)],
	exports: [RouterModule ]
})

export class AppRoutingModule {

}