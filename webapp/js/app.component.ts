import { Component, ViewChild } from '@angular/core';
import { MenuComponent } from './frontend/menu/menu.component';

@Component({
    selector: 'my-app',
    template: '<router-outlet></router-outlet>'
})

export class AppComponent {
	
}
