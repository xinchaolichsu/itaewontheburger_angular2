// nhiem vu chinh cua file nay nay la import nhieu thanh phan khac cua 1 nhom
// vi du: có thể export cai nhu MenuModel, MenuService 
// cac cho khac chi can goi file index nay la co the su dung cac thanh phan ma khong phai import nhieu lan
// gia su co tao them 1 thu muc nua thi cung dc export vao day 
// vi du 
// 			export * from './menu.service';
// 			export * from './menu.model';
//
// va khi goi file index nay ra de dung thi - import { MenuService, MenuModel, MenuComponent } from './index';

export * from './menu.component';
