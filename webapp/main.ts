import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule }              from './js/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);

